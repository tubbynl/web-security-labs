package com.jdriven.hacks.controller;

import java.net.URISyntaxException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/templates")
public class BadTemplateController {

    @RequestMapping("/xss")
    public ResponseEntity<String> badTemplate() throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Access-Control-Allow-Origin", "*");
        return new ResponseEntity<String>(
                "<div>{{sticky.content}}<span class=\"signed\">{{sticky.username}}</span></div>"
                        + String.format(StickyPosterController.XSS_STICKY_FORMAT,
                                "Ik ben weer gehacked, door dynamic includes!!"), headers, HttpStatus.OK);
    }

}
