package com.jdriven.hacks.controller;

import java.net.URI;
import java.net.URISyntaxException;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BadImageController {

    @RequestMapping("/badimg/smiley.png")
    public ResponseEntity<Void> badSmiley() throws URISyntaxException {
        HttpHeaders headers = new HttpHeaders();
        headers.setLocation(new URI("http://localhost:8080/sticky/search/bad"));
        return new ResponseEntity<Void>(headers, HttpStatus.SEE_OTHER);
    }
}
